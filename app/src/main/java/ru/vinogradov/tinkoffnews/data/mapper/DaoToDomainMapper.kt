package ru.vinogradov.tinkoffnews.data.mapper

import ru.vinogradov.tinkoffnews.data.model.NewsContentEntity
import ru.vinogradov.tinkoffnews.data.model.NewsTitleEntity
import ru.vinogradov.tinkoffnews.domain.model.NewsContent
import ru.vinogradov.tinkoffnews.domain.model.NewsTitle
import java.text.SimpleDateFormat
import java.util.*

fun NewsTitleEntity.mapToDomain() = NewsTitle(
        id = id,
        title = text,
        publicationDate = getDateString(publicationDate.milliseconds)
)

fun NewsContentEntity.mapToDomain() = NewsContent(
        title = title.text,
        publicationDate = getDateString(title.publicationDate.milliseconds),
        content = content
)

private val DATE_PATTERN = SimpleDateFormat("H:mm dd.MM.yyy", Locale.US)

private fun getDateString(timestamp: Long) = DATE_PATTERN.format(Date(timestamp))