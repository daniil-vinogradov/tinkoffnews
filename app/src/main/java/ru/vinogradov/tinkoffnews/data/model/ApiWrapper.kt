package ru.vinogradov.tinkoffnews.data.model

import com.google.gson.annotations.SerializedName

data class ApiWrapper<T>(
        @SerializedName("resultCode") val resultCode: String,
        @SerializedName("payload") val payload: T
)