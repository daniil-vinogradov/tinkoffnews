package ru.vinogradov.tinkoffnews.data.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class DateEntity(
        @ColumnInfo(name = "publication_date")
        @SerializedName("milliseconds") val milliseconds: Long
)