package ru.vinogradov.tinkoffnews.data.model

import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["id"])
data class NewsContentEntity(
        @Embedded @SerializedName("title") val title: NewsTitleEntity,
        @SerializedName("content") val content: String
)