package ru.vinogradov.tinkoffnews.data.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class NewsTitleEntity(
        @PrimaryKey @SerializedName("id") val id: Long,
        @SerializedName("name") val name: String,
        @SerializedName("text") val text: String,
        @Embedded @SerializedName("publicationDate") val publicationDate: DateEntity,
        @SerializedName("bankInfoTypeId") val bankInfoTypeId: Int
)