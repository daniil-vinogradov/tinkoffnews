package ru.vinogradov.tinkoffnews.data.repository

import androidx.room.EmptyResultSetException
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleTransformer
import ru.vinogradov.tinkoffnews.data.mapper.mapToDomain
import ru.vinogradov.tinkoffnews.data.model.ApiWrapper
import ru.vinogradov.tinkoffnews.data.room.NewsDao
import ru.vinogradov.tinkoffnews.data.server.TinkoffNewsService
import ru.vinogradov.tinkoffnews.domain.ApiException
import ru.vinogradov.tinkoffnews.domain.EmptyCacheException
import ru.vinogradov.tinkoffnews.domain.model.NewsTitle
import ru.vinogradov.tinkoffnews.domain.repository.NewsRepository

private const val RESULT_CODE_OK = "OK"

open class NewsRepositoryImpl(
        private val service: TinkoffNewsService,
        private val newsDao: NewsDao
) : NewsRepository {

    override fun getNewsListFromServer() = service
            .getListNews()
            .compose(checkForServerErrorAndUnwrap())
            .doOnSuccess { newsDao.insertNewsTitles(it) }
            .flatMapObservable { Observable.fromIterable(it) }
            .sorted { o1, o2 ->
                val timestamp1 = o1.publicationDate.milliseconds
                val timestamp2 = o2.publicationDate.milliseconds
                -timestamp1.compareTo(timestamp2)
            }
            .map { it.mapToDomain() }
            .toList()!!

    override fun getNewsListFromDatabase(): Single<List<NewsTitle>> = newsDao
            .getAllNewsTitle()
            .flatMapObservable {
                if (it.isEmpty()) throw EmptyCacheException()
                Observable.fromIterable(it)
            }
            .map { it.mapToDomain() }
            .toList()!!

    override fun getNewsContentFromServer(id: Long) = service
            .getNewsContent(id)
            .compose(checkForServerErrorAndUnwrap())
            .doOnSuccess { newsDao.insertNewsContent(it) }
            .map { it.mapToDomain() }!!

    override fun getNewsContentFromDatabase(id: Long) = newsDao
            .getNewsContent(id)
            .onErrorResumeNext {
                if (it is EmptyResultSetException)
                    Single.error(EmptyCacheException())
                else Single.error(it)
            }
            .map { it.mapToDomain() }!!

    private fun <T> checkForServerErrorAndUnwrap(): SingleTransformer<ApiWrapper<T>, T> {
        return SingleTransformer {
            it.map { response ->
                if (response.resultCode != RESULT_CODE_OK) throw ApiException()
                response.payload
            }
        }
    }
}