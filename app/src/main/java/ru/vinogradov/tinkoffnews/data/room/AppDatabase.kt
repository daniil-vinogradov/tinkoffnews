package ru.vinogradov.tinkoffnews.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ru.vinogradov.tinkoffnews.data.model.NewsContentEntity
import ru.vinogradov.tinkoffnews.data.model.NewsTitleEntity


@Database(entities = [NewsTitleEntity::class, NewsContentEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao

    companion object Factory {
        private var database: AppDatabase? = null

        fun getDatabase(appContext: Context): AppDatabase {
            if (database == null) {
                database = Room.databaseBuilder(appContext,
                        AppDatabase::class.java, "database-name").build()
            }
            return database!!
        }
    }
}