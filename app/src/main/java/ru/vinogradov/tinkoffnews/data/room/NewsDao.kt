package ru.vinogradov.tinkoffnews.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import ru.vinogradov.tinkoffnews.data.model.NewsContentEntity
import ru.vinogradov.tinkoffnews.data.model.NewsTitleEntity


@Dao
interface NewsDao {

    @Query("SELECT * FROM newstitleentity ORDER BY publication_date DESC")
    fun getAllNewsTitle(): Single<List<NewsTitleEntity>>

    @Query("SELECT * FROM newscontententity WHERE id = :id")
    fun getNewsContent(id: Long): Single<NewsContentEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewsTitles(newsTitles: List<NewsTitleEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewsContent(newsContent: NewsContentEntity)
}