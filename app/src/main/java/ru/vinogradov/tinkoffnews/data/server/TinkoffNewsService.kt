package ru.vinogradov.tinkoffnews.data.server

import com.google.gson.Gson
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import ru.vinogradov.tinkoffnews.data.model.ApiWrapper
import ru.vinogradov.tinkoffnews.data.model.NewsContentEntity
import ru.vinogradov.tinkoffnews.data.model.NewsTitleEntity


interface TinkoffNewsService {

    @GET("v1/news")
    fun getListNews(): Single<ApiWrapper<List<NewsTitleEntity>>>

    @GET("v1/news_content")
    fun getNewsContent(@Query("id") id: Long): Single<ApiWrapper<NewsContentEntity>>

    companion object Factory {
        val getService: TinkoffNewsService by lazy {
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.tinkoff.ru/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .build()
            retrofit.create(TinkoffNewsService::class.java)
        }
    }
}