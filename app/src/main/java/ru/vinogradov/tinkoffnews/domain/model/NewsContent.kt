package ru.vinogradov.tinkoffnews.domain.model

data class NewsContent(
        val title: String,
        val publicationDate: String,
        val content: String
)