package ru.vinogradov.tinkoffnews.domain.model

data class NewsTitle(
        val id: Long,
        val title: String,
        val publicationDate: String
)