package ru.vinogradov.tinkoffnews.domain.repository

import io.reactivex.Single
import ru.vinogradov.tinkoffnews.domain.model.NewsContent
import ru.vinogradov.tinkoffnews.domain.model.NewsTitle

interface NewsRepository {

    fun getNewsListFromServer(): Single<List<NewsTitle>>

    fun getNewsListFromDatabase(): Single<List<NewsTitle>>

    fun getNewsContentFromServer(id: Long): Single<NewsContent>

    fun getNewsContentFromDatabase(id: Long): Single<NewsContent>
}