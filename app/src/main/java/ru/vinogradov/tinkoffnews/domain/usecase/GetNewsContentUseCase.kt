package ru.vinogradov.tinkoffnews.domain.usecase

import io.reactivex.Single
import ru.vinogradov.tinkoffnews.domain.EmptyCacheException
import ru.vinogradov.tinkoffnews.domain.model.NewsContent
import ru.vinogradov.tinkoffnews.domain.repository.NewsRepository

class GetNewsContentUseCase(private val newsRepository: NewsRepository) {

    fun execute(id: Long, refresh: Boolean = false): Single<NewsContent> {
        return if (refresh) {
            newsRepository.getNewsContentFromServer(id)
        } else {
            newsRepository.getNewsContentFromDatabase(id)
                    .onErrorResumeNext {
                        if (it is EmptyCacheException)
                            newsRepository.getNewsContentFromServer(id)
                        else Single.error(it)
                    }
        }
    }
}