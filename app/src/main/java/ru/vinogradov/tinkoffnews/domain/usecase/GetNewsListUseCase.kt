package ru.vinogradov.tinkoffnews.domain.usecase

import io.reactivex.Single
import ru.vinogradov.tinkoffnews.domain.EmptyCacheException
import ru.vinogradov.tinkoffnews.domain.model.NewsTitle
import ru.vinogradov.tinkoffnews.domain.repository.NewsRepository

class GetNewsListUseCase(private val newsRepository: NewsRepository) {

    fun execute(refresh: Boolean = false): Single<List<NewsTitle>> {
        return if (refresh) {
            newsRepository.getNewsListFromServer()
        } else {
            newsRepository.getNewsListFromDatabase()
                    .onErrorResumeNext {
                        if (it is EmptyCacheException)
                            newsRepository.getNewsListFromServer()
                        else Single.error(it)
                    }
        }
    }
}