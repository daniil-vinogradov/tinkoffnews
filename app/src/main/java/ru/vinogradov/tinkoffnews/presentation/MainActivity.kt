package ru.vinogradov.tinkoffnews.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.vinogradov.tinkoffnews.R
import ru.vinogradov.tinkoffnews.presentation.common.Router
import ru.vinogradov.tinkoffnews.presentation.newsdetail.NewsDetailFragment
import ru.vinogradov.tinkoffnews.presentation.newslist.NewsListFragment

class MainActivity : AppCompatActivity(), Router {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, NewsListFragment())
                    .commit()
        }
    }

    override fun openNewsDetail(id: Long) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, NewsDetailFragment.create(id))
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }
}
