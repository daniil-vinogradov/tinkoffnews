package ru.vinogradov.tinkoffnews.presentation.common


import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.vinogradov.tinkoffnews.presentation.common.moxy.MvpAndroidXFragment

abstract class BaseFragment : MvpAndroidXFragment() {

    abstract val layout: Int

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?) =
            inflater.inflate(layout, container, false)!!
}