package ru.vinogradov.tinkoffnews.presentation.common

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface LoadingView : MvpView {

    fun showLoading()

    fun hideLoading()
}