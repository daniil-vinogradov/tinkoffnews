package ru.vinogradov.tinkoffnews.presentation.common

interface Router {
    fun openNewsDetail(id: Long)
}