package ru.vinogradov.tinkoffnews.presentation.newsdetail

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fr_news_detail.*
import ru.vinogradov.tinkoffnews.R
import ru.vinogradov.tinkoffnews.data.repository.NewsRepositoryImpl
import ru.vinogradov.tinkoffnews.data.room.AppDatabase
import ru.vinogradov.tinkoffnews.data.server.TinkoffNewsService
import ru.vinogradov.tinkoffnews.domain.model.NewsContent
import ru.vinogradov.tinkoffnews.domain.usecase.GetNewsContentUseCase
import ru.vinogradov.tinkoffnews.presentation.common.BaseFragment

class NewsDetailFragment : BaseFragment(), NewsDetailView {

    override val layout = R.layout.fr_news_detail

    @InjectPresenter
    lateinit var presenter: NewsDetailPresenter

    //TODO Implement DI framework
    @ProvidePresenter
    fun providePresenter(): NewsDetailPresenter =
            NewsDetailPresenter(arguments!!.getLong(ARG_NEWS_ID),
                    GetNewsContentUseCase(
                            NewsRepositoryImpl(
                                    TinkoffNewsService.getService,
                                    AppDatabase.getDatabase(context!!.applicationContext).newsDao())))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        swipeRefreshLayout.setOnRefreshListener { presenter.refresh() }
    }

    override fun showNews(news: NewsContent) {
        title.text = news.title
        date.text = news.publicationDate
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            content.text = Html.fromHtml(news.content, Html.FROM_HTML_MODE_LEGACY)
        } else {
            content.text = Html.fromHtml(news.content)
        }
    }

    override fun showToast(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    companion object {
        private const val ARG_NEWS_ID = "arg_news_id"
        fun create(id: Long) =
                NewsDetailFragment().apply {
                    arguments = Bundle().apply {
                        putLong(ARG_NEWS_ID, id)
                    }
                }
    }
}