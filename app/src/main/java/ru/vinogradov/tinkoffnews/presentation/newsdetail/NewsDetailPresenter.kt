package ru.vinogradov.tinkoffnews.presentation.newsdetail

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.vinogradov.tinkoffnews.domain.model.NewsContent
import ru.vinogradov.tinkoffnews.domain.usecase.GetNewsContentUseCase

@InjectViewState
class NewsDetailPresenter(
        private val newsId: Long,
        private val getNewsContentUseCase: GetNewsContentUseCase
) : MvpPresenter<NewsDetailView>() {

    private var disposable: Disposable? = null

    override fun onFirstViewAttach() {
        handleSingle(getNewsContentUseCase.execute(newsId))
    }

    fun refresh() {
        handleSingle(getNewsContentUseCase.execute(newsId, refresh = true))
    }

    private fun handleSingle(single: Single<NewsContent>) {
        disposable?.dispose()
        disposable = single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showLoading() }
                .doAfterTerminate { viewState.hideLoading() }
                .subscribe({ res -> viewState.showNews(res) }, { t ->
                    viewState.showToast(t.message ?: "Error")
                })
    }

    override fun onDestroy() {
        disposable?.dispose()
    }
}