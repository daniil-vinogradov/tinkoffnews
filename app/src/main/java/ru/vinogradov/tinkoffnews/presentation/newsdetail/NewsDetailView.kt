package ru.vinogradov.tinkoffnews.presentation.newsdetail

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.vinogradov.tinkoffnews.domain.model.NewsContent
import ru.vinogradov.tinkoffnews.presentation.common.LoadingView

interface NewsDetailView : LoadingView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showNews(news: NewsContent)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showToast(text: String)
}