package ru.vinogradov.tinkoffnews.presentation.newslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fr_news_list.*
import kotlinx.android.synthetic.main.item_news.*
import ru.vinogradov.tinkoffnews.R
import ru.vinogradov.tinkoffnews.data.repository.NewsRepositoryImpl
import ru.vinogradov.tinkoffnews.data.room.AppDatabase
import ru.vinogradov.tinkoffnews.data.server.TinkoffNewsService
import ru.vinogradov.tinkoffnews.domain.model.NewsTitle
import ru.vinogradov.tinkoffnews.domain.usecase.GetNewsListUseCase
import ru.vinogradov.tinkoffnews.presentation.common.BaseFragment
import ru.vinogradov.tinkoffnews.presentation.common.Router

class NewsListFragment : BaseFragment(), NewsListView {

    override val layout = R.layout.fr_news_list

    private val adapter = NewsAdapter()

    private val router by lazy {
        activity as Router
    }

    @InjectPresenter
    lateinit var presenter: NewsListPresenter

    //TODO Implement DI framework
    @ProvidePresenter
    fun providePresenter(): NewsListPresenter =
            NewsListPresenter(
                    GetNewsListUseCase(
                            NewsRepositoryImpl(
                                    TinkoffNewsService.getService,
                                    AppDatabase.getDatabase(context!!.applicationContext).newsDao())))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        swipeRefreshLayout.setOnRefreshListener { presenter.refresh() }
        newsRecyclerView.layoutManager = LinearLayoutManager(context)
        newsRecyclerView.adapter = adapter
    }

    override fun showNews(news: List<NewsTitle>) = adapter.showNews(news)

    override fun showToast(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    private inner class NewsAdapter : RecyclerView.Adapter<NewsItemViewHolder>() {

        private var news: List<NewsTitle>? = null

        fun showNews(news: List<NewsTitle>) {
            this.news = news
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): NewsItemViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
            return NewsItemViewHolder(view)
        }

        override fun getItemCount(): Int = news?.size ?: 0
        override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) =
                holder.bind(news?.get(position)!!)
    }

    private inner class NewsItemViewHolder(
            override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        private var currItem: NewsTitle? = null

        init {
            containerView.setOnClickListener { view ->
                if (adapterPosition == RecyclerView.NO_POSITION) {
                    return@setOnClickListener
                }
                currItem?.let {
                    router.openNewsDetail(it.id)
                }
            }
        }

        fun bind(newsTitle: NewsTitle) {
            currItem = newsTitle
            title.text = newsTitle.title
            date.text = newsTitle.publicationDate
        }
    }
}