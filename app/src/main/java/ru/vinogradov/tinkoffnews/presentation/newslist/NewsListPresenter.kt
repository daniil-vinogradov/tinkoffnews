package ru.vinogradov.tinkoffnews.presentation.newslist

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.vinogradov.tinkoffnews.domain.model.NewsTitle
import ru.vinogradov.tinkoffnews.domain.usecase.GetNewsListUseCase

@InjectViewState
class NewsListPresenter(
        private val getNewsListUseCase: GetNewsListUseCase
) : MvpPresenter<NewsListView>() {

    private var disposable: Disposable? = null

    override fun onFirstViewAttach() {
        handleSingle(getNewsListUseCase.execute())
    }

    fun refresh() {
        handleSingle(getNewsListUseCase.execute(refresh = true))
    }

    private fun handleSingle(single: Single<List<NewsTitle>>) {
        disposable?.dispose()
        disposable = single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showLoading() }
                .doAfterTerminate { viewState.hideLoading() }
                .subscribe({ res -> viewState.showNews(res) }, { t ->
                    viewState.showToast(t.message ?: "Error")
                })
    }

    override fun onDestroy() {
        disposable?.dispose()
    }
}