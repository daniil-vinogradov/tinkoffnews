package ru.vinogradov.tinkoffnews.presentation.newslist

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.vinogradov.tinkoffnews.domain.model.NewsTitle
import ru.vinogradov.tinkoffnews.presentation.common.LoadingView

interface NewsListView : LoadingView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showNews(news: List<NewsTitle>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showToast(text: String)
}